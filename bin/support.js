const Telegraf = require('telegraf');
const {Extra} = Telegraf;
const Markup = require('telegraf/markup')
const config = require('../config.js');
const handler = require('./ticket_handler.js');
let cache = require('./cache.js');
var dbhandler = require('./dbhandler.js');

const bot = new Telegraf(config.bot_token);

const cron = require('cron');
const exec = require('child_process').exec;
let cronJob;

cache.html = Extra.HTML(); // eslint-disable-line no-use-before-define
cache.markdown = Extra.markdown();
cache.noSound = Extra
      .HTML().notifications(false); // eslint-disable-line no-use-before-define


const escapeHtml = s => s
    .replace(/&/g, '&amp;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#39;')
    .replace(/</g, '&lt;');


const link = ({ id, first_name }) =>
    `<a href="tg://user?id=${id}">${escapeHtml(first_name)}</a>`;


const root = Extra.HTML().markup(( // eslint-disable-line no-use-before-define
  m // inline keyboard for admin dashboard
) =>
  m.inlineKeyboard([
    m.callbackButton('🔄 Update', 'update'),
    m.callbackButton('📖 Log', 'log'),
    m.callbackButton('♻️ Restart', 'restart'),
    m.callbackButton('🚫 Stop', 'stop'),
  ])
);

bot.action('restart', (ctx) => {
  // restart other bot
  if (ctx.from.id === config.owner_id) {
    ex('service ' + config.supported_bot + ' restart', function(results) {
      setTimeout(function() {
        ex('service ' + config.supported_bot + ' status', function(results) {
          if (cronJob !== undefined) {
            status = cronJob.running;
          }
          ctx.editMessageText(
            'Current status:\n' + results + '\nCron running: restart',
            root
          );
        });
      }, 2000);
    });
    if (cronJob !== undefined) {
      if (cronJob.running === false) {
        cronJob.start();
      }
    }
  }
});
bot.action('log', (ctx) => {
  // send other bots log
  if (ctx.from.id == config.owner_id) {
    ex(
      'journalctl -u ' + config.supported_bot + ' -b > /logs/log.txt',
      function(results) {
        ctx.replyWithDocument({
          source: '/logs/log.txt',
        });
      }
    );
  }
});
bot.action('update', (ctx) => {
  // update admin dasboard"s status
  if (ctx.from.id == config.owner_id) {
    let status;
    ex('service ' + config.supported_bot + ' status', function(results) {
      if (cronJob !== undefined) {
        status = cronJob.running;
      }
      ctx.editMessageText(
        'Current status:\n' + results + '\nCron running: ' + status,
        root
      );
    });
  }
});
bot.action('stop', (ctx) => {
  // stop the bot
  if (ctx.from.id == config.owner_id) {
    ex('service ' + config.supported_bot + ' stop', function(results) {
      ctx.editMessageText('Bitgram stopped', root);
    });
    if (cronJob !== undefined) {
      if (cronJob.running === true) {
        cronJob.stop();
      }
    }
  }
});


bot.action('main', async (ctx) => {
  ctx.answerCbQuery()

  ctx.editMessageText(
      config.hello,
      Extra.HTML().markup(Markup.inlineKeyboard([
              [Markup.urlButton('Telegram Nasıl Kullanılmalı?', 'https://giphy.com/gifs/f5HJLMJjp55HW/html5')],
              [Markup.callbackButton('🤖  Bot Rehberi', 'müzik'), Markup.callbackButton('🎞  Dizi/Film', 'oyun')],
              [Markup.callbackButton('💢  Rastgele', 'grup'), Markup.callbackButton('🏳️‍🌈  Ortaya Karışık', 'video')],
              [Markup.callbackButton('👾  Oyunlar', 'list'), Markup.callbackButton('🎴  Sticker', 'sticker')],
          ]))
          .webPreview(false)
  )
      .catch((err) => sendError(err, ctx))
})


bot.action('müzik', async (ctx) => {
  ctx.answerCbQuery()

  ctx.editMessageText(
    config.müzik,
    Extra.HTML().markup(Markup.inlineKeyboard([
      [Markup.callbackButton('Geri', 'main'), Markup.urlButton('Açıklama', 'https://telegra.ph/Bot-Rehberi-11-21')],
    ]))
    .webPreview(false)
  )
    .catch((err) => sendError(err, ctx))
})



bot.action('oyun', async (ctx) => {
  ctx.answerCbQuery()

  ctx.editMessageText(
      config.oyun,
      Extra.HTML().markup(Markup.inlineKeyboard([
            [Markup.callbackButton('Geri', 'main'), Markup.urlButton('Açıklama', 'https://telegra.ph/DiziFilm-11-21')],
          ]))
          .webPreview(false)
  )
      .catch((err) => sendError(err, ctx))
})



bot.action('video', async (ctx) => {
  ctx.answerCbQuery()

  ctx.editMessageText(
      config.video,
      Extra.HTML().markup(Markup.inlineKeyboard([
            [Markup.callbackButton('Geri', 'main'), Markup.urlButton('Açıklama', 'https://telegra.ph/Ortaya-Karışık-11-21')],
          ]))
          .webPreview(false)
  )
      .catch((err) => sendError(err, ctx))
})


bot.action('grup', async (ctx) => {
  ctx.answerCbQuery()

  ctx.editMessageText(
      config.grup,
      Extra.HTML().markup(Markup.inlineKeyboard([
            [Markup.callbackButton('Geri', 'main'), Markup.urlButton('Açıklama', 'https://telegra.ph/Rastgele-11-21')],
          ]))
          .webPreview(false)
  )
      .catch((err) => sendError(err, ctx))
})



bot.action('list', async (ctx) => {
  ctx.answerCbQuery()

  ctx.editMessageText(
      config.list,
      Extra.HTML().markup(Markup.inlineKeyboard([
            [Markup.callbackButton('Geri', 'main'), Markup.urlButton('Açıklama', 'https://telegra.ph/Oyunlar-11-21')],
          ]))
          .webPreview(false)
  )
      .catch((err) => sendError(err, ctx))
})



bot.action('sticker', async (ctx) => {
  ctx.answerCbQuery()

  ctx.editMessageText(
      config.sticker,
      Extra.HTML().markup(Markup.inlineKeyboard([
            [Markup.callbackButton('Geri', 'main'), Markup.urlButton('Açıklama', 'https://telegra.ph/Sticker-11-21')],
          ]))
          .webPreview(false)
  )
      .catch((err) => sendError(err, ctx))
})




let cronSession = function(ctx) {
  // check every 5 seconds if other bot is down, if it"s inactive restart it
  console.log('Session started.\n');
  cronJob = cron.job(
    '*/5 * * * * *',
    function() {
      // 5 seconds
      ex('systemctl is-active ' + config.supported_bot + '', function(results) {
        if (results.indexOf('failed') > -1) {
          // restart on failed
          ex(
            'journalctl -u ' +
              config.supported_bot +
              ' -b > /var/www/cache.html/' +
              config.supported_bot +
              '/logs/log.txt',
            function(results) {
              ctx.replyWithDocument({
                source:
                  '/var/www/cache.html/' +
                  config.supported_bot +
                  '/logs/log.txt',
              });
            }
          );
          ex('service ' + config.supported_bot + ' start', function(results) {
            bot.telegram.sendMessage(
              config.staffchat_id,
              'Restarted bot. See log.',
              cache.html
            );
          });
        }
        if (results.indexOf('inactive') > -1) {
          // restart on inactive
          ex(
            'journalctl -u ' +
              config.supported_bot +
              ' -b > /var/www/cache.html/' +
              config.supported_bot +
              '/logs/log.txt',
            function(results) {
              ctx.replyWithDocument({
                source:
                  '/var/www/cache.html/' +
                  config.supported_bot +
                  '/logs/log.txt',
              });
            }
          );
          ex('service ' + config.supported_bot + ' start', function(results) {
            bot.telegram.sendMessage(
              config.staffchat_id,
              'Restarted bot. See log.',
              cache.html
            );
          });
        }
        results = null;
      });
    },
    function() {
      bot.telegram.sendMessage(
        config.staffchat_id,
        'Stopped cron job.',
        cache.html
      );
    }
  );
  cronJob.start();
};

let ex = function execute(command, callback) {
  // execute command
  exec(command, function(error, stdout, stderr) {
    callback(stdout);
  });
};


bot.start( async (ctx) => {
    if (!ctx.chat.type.endsWith('private')) return null;

    await ctx.replyWithDocument("CAADBAADgQADhEa6GkfFaySZsIU4FgQ", Markup
      .keyboard([
        ['🔍 Kategoriler', '❔ Hakkımızda'],
      ]).resize()
      .extra()
  )
  return ctx.replyWithHTML(`<b>Merhaba</b> ${link(ctx.from)}`)
})



bot.hears('📞 İletişim', ({ // on start reply with chat bot rules
  reply, from, chat}) => {
  reply(config.startCommandText, cache.html, cache.html);
});


bot.hears("🔍 Kategoriler", async (ctx) => {
    if (!ctx.chat.type.endsWith('private')) return null;
    try {
        ctx.reply(
            config.hello,
            Extra.HTML().markup(Markup.inlineKeyboard([
              [Markup.urlButton('Telegram Nasıl Kullanılmalı?', 'https://telegram.org/faq/tr')],
              [Markup.callbackButton('🤖  Bot Rehberi', 'müzik'), Markup.callbackButton('🎞  Dizi/Film', 'oyun')],
              [Markup.callbackButton('💢  Rastgele', 'grup'), Markup.callbackButton('🏳️‍🌈  Ortaya Karışık', 'video')],
              [Markup.callbackButton('👾  Oyunlar', 'list'), Markup.callbackButton('🎴  Sticker', 'sticker')],
                ]))
                .webPreview(false)
        )
    } catch (err) {
        sendError(err, ctx)
    }
})


bot.hears("🔍", async (ctx) => {
    if (!ctx.chat.type.endsWith('group')) return null;
    try {
        ctx.reply(
            config.hello,
            Extra.HTML().markup(Markup.inlineKeyboard([
              [Markup.urlButton('Telegram Nasıl Kullanılmalı?', 'https://telegram.org/faq/tr')],
              [Markup.callbackButton('🤖  Bot Rehberi', 'müzik'), Markup.callbackButton('🎞  Dizi/Film', 'oyun')],
              [Markup.callbackButton('💢  Rastgele', 'grup'), Markup.callbackButton('🏳️‍🌈  Ortaya Karışık', 'video')],
              [Markup.callbackButton('👾  Oyunlar', 'list'), Markup.callbackButton('🎴  Sticker', 'sticker')],
                ]))
                .webPreview(false)
        )
    } catch (err) {
        sendError(err, ctx)
    }
})


bot.hears("💬 Sohbet", async (ctx) => {
    if (!ctx.chat.type.endsWith('private')) return null;
    try {
        ctx.replyWithHTML('<a href="https://t.me/botsohbet">Sohbet Grubu</a>')


    } catch (err) {
        sendError(err, ctx)
    }
})


bot.hears("❔ Hakkımızda", async (ctx) => {
    if (!ctx.chat.type.endsWith('private')) return null;
    try {
        ctx.replyWithHTML('<b>Biz Kimiz?</b>\nTelegram diğer sohbet uygulamalarına göre daha fonksiyonlu bir platform.Biz de ekip arkadaşlarımızla farklı kategorilerde grup ve kanallar açarak bu platformu keyifli hale getirmeye çalışıyoruz.\n\nFarklı fikir ve öneriler için; @kubraa @holytotem')


    } catch (err) {
        sendError(err, ctx)
    }
})


async function sendError(err, ctx) {
  console.log(err.toString())
  if (ctx != undefined) {
    if (err.code === 400) {
      return setTimeout(() => {
        ctx.answerCbQuery()
        ctx.editMessageText(
          text.hello,
          Extra.HTML().markup(Markup.inlineKeyboard([
              [Markup.urlButton('Telegram Nasıl Kullanılmalı?', 'https://telegram.org/faq/tr')],
              [Markup.callbackButton('🤖  Bot Rehberi', 'müzik'), Markup.callbackButton('🎞  Dizi/Film', 'oyun')],
              [Markup.callbackButton('💢  Rastgele', 'grup'), Markup.callbackButton('🏳️‍🌈  Ortaya Karışık', 'video')],
              [Markup.callbackButton('👾  Oyunlar', 'list'), Markup.callbackButton('🎴  Sticker', 'sticker')],
          ]))
          .webPreview(false)
        )
      }, 500)
    } else if (err.code === 429) {
      return ctx.editMessageText(
        'Bakımda' +
        '123'
      )
    }

    bot.telegram.sendMessage(data.admins[0], '[' + ctx.from.first_name + '](tg://user?id=' + ctx.from.id + ') has got an error.\nError text: ' + err.toString(), {parse_mode: 'markdown'})
  } else {
    bot.telegram.sendMessage(data.admins[0], 'There`s an error:' + err.toString())
  }
}

bot.catch((err) => {
  sendError(err)
})

process.on('uncaughtException', (err) => {
  sendError(err)
})












bot.command('id', ({reply, from, chat}) => {
  reply(from.id + ' ' + chat.id);
});
bot.command('faq', (ctx) => {
  // faq
  ctx.reply(config.faqCommandText, cache.html);
});

bot.command('root', (ctx) => {
  // admin dashboard can only be used by owner
  console.log('id ' + ctx.from.id);
  if (ctx.from.id.toString() == config.owner_id) {
    bot.telegram.sendMessage(
      config.staffchat_id,
      'You will receive the logs when the bot crashes.',
      root
    );
    cronSession(ctx);
  }
});

bot.telegram.getMe().then((botInfo) => {
  // enable for groups (get own username)
  bot.options.username = botInfo.username;
});

const downloadPhotoMiddleware = (ctx, next) => {
  // download photos
  return bot.telegram.getFileLink(ctx.message.photo[0]).then((link) => {
    ctx.state.fileLink = link;
    return next();
  });
};

const downloadVideoMiddleware = (ctx, next) => {
  // download videos
  return bot.telegram.getFileLink(ctx.message.video).then((link) => {
    ctx.state.fileLink = link;
    return next();
  });
};

const downloadDocumentMiddleware = (ctx, next) => {
  // download documents
  console.log(ctx.message);
  return bot.telegram.getFileLink(ctx.message.document).then((link) => {
    ctx.state.fileLink = link;
    return next();
  });
};

// display open tickets
bot.command('open', (ctx) => {
  ctx.getChat().then(function(chat) {
    if (chat.id.toString() === config.staffchat_id) {
      console.log('chatid', chat.id.toString());
      ctx.getChatAdministrators().then(function(admins) {
        admins = JSON.stringify(admins);
        if (admins.indexOf(ctx.from.id) > -1) {

          dbhandler.open(function(userList) {
            console.log(userList);
            let openTickets = '';

            for (let i in userList) {
              if (userList[i]['userid'] !== null && userList[i]['userid'] !== undefined) {
                openTickets += '<code>#t' + userList[i]['userid'].toString() + '</code>\n';
              }
            }
            setTimeout(function() {
              bot.telegram.sendMessage(
                chat.id,
                '<b>Open Tickets:\n\n</b>' + openTickets,
                cache.noSound
              );
            }, 10);

          });
          
        }
      });
    }
  });
});

// close ticket
bot.command('close', (ctx) => {
  ctx.getChat().then(function(chat) {
    if (chat.id.toString() === config.staffchat_id) {
      ctx.getChatAdministrators().then(function(admins) {
        admins = JSON.stringify(admins);
        if (
          ctx.message.reply_to_message !== undefined &&
          admins.indexOf(ctx.from.id) > -1
        ) {
          let replyText = ctx.message.reply_to_message.text;
          let userid = replyText.match(new RegExp('#t' + '(.*)' + ' ' + config.lang_from));
          
          dbhandler.add(userid[1], "closed")
          bot.telegram.sendMessage(
            chat.id,
            'Ticket <code>#t'+userid[1]+'</code> closed',
            cache.noSound
          );
        }
      });
    }
  });
});


// ban user
bot.command('ban', (ctx) => {
  ctx.getChat().then(function(chat) {
    if (chat.id.toString() === config.staffchat_id) {
      ctx.getChatAdministrators().then(function(admins) {
        admins = JSON.stringify(admins);
        if (
          ctx.message.reply_to_message !== undefined &&
          admins.indexOf(ctx.from.id) > -1
        ) {
          let replyText = ctx.message.reply_to_message.text;
          let userid = replyText.match(new RegExp('#t' + '(.*)' + ' ' + config.lang_from));
          
          dbhandler.add(userid[1], "banned")
          bot.telegram.sendMessage(
            chat.id,
            config.lang_usr_with_ticket + ' <code>#t'+userid[1]+'</code> ' + config.lang_banned,
            cache.noSound
          );
        }
      });
    }
  });
});

// handle photo input
bot.on('photo', downloadPhotoMiddleware, (ctx, next) => {
  handler.photo(bot, ctx);
});

// handle video input
bot.on('video', downloadVideoMiddleware, (ctx, next) => {
  handler.video(bot, ctx);
});

// handle file input
bot.on('document', downloadDocumentMiddleware, (ctx, next) => {
  handler.document(bot, ctx);
});

bot.hears(/(.+)/, (ctx) => handler.ticket(bot, ctx));

// telegraf error handling
bot.catch((err) => {
  console.log('Error: ', err)
})

bot.startPolling();
/*
If you receive Error: 409: Conflict: can't use getUpdates method while
webhook is active, comment bot.startPolling() out, remove // of the following
commands, run your bot once and undo the changes. This will disable the
webhook by setting it to empty.

bot.telegram.setWebhook("");
bot.startWebhook("")
*/
