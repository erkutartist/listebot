const { toLambda } = require('probot-serverless-now');

const app = require('../bin/support.js');

module.exports = toLambda(app);
