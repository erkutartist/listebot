module.exports = {
  // bot ayarları
  bot_token: '1005048694:AAFz1AHY2-0xsYidyFMSDy9Aouuzv9rYwq4', // you can get it in @BotFather
  mongoLink: 'mongodb+srv://dizifilm:1135148a@cluster0-egfe0.mongodb.net/test?retryWrites=true&w=majority',// support bot token
  staffchat_id: '-1001253342868', // eg. -123456789
  owner_id: '448415364',
  supported_bot: 'deneme', // service name of the supported bot
  spam_time: 5 * 60 * 1000, // time (in MS) in which user may send 5 messages

  // destek
  startCommandText: 'Destek hattımıza hoş geldiniz! Sorunuzu buradan ya da @dizifilms hesabından sorun.',
  faqCommandText: 'Destek',
  lang_contactMessage:
    `Bizimle iletişime geçtiğiniz için teşekkürler. En kısa zamanda cevaplayacağız.`,
  lang_blockedSpam:
    `Son zamanlarda epeyce soru gönderdiniz.
    Lütfen sakinleşin ve personel tarafından incelenene kadar bekleyin.`,
  lang_ticket: 'Ticket Atıldı',
  lang_acceptedBy: 'tarafından kabul edildi',
  lang_dear: 'Sayın',
  lang_regards: '- Yönetici',
  lang_from: 'kullanıcı',
  lang_language: 'Dil',
  lang_msg_sent: 'Mesaj kullanıcıya gönderildi.',
  lang_usr_with_ticket: 'Ticket numarası',
  lang_banned: 'olan kullanıcının bildirim göndermesi yasaklandı.',
  // Yazılar
  hello: '🗂 <b>Lütfen bir kategori seçin.</b>',
  müzik: '🤖 Bot Rehberi:\n\n<a href="t.me/botarsivi">Türkçe Botlar</a>\n\<a href="t.me/botsohbet">Türkçe Botlar Sohbet</a>\n<a href="t.me/arsivbot">Bot Arşiv</a>\n<a href="t.me/federasyonbot">Federasyon Botu</a>',
  oyun: '🎞 Dizi/Film:\n\n<a href="t.me/dizifilm">Yabancı Dizi/Film</a>\n<a href="t.me/yabancifilm">Film Arşivi</a>\n<a href="t.me/dizifilms">Dizi Film Operatör</a>\n<a href="t.me/dizifilmarsiv">Dizi/Film Arşiv</a>\n<a href="t.me/dizifilmlinkleri">Dizi/Film Linkleri</a>\n<a href="t.me/dizifilmrobot">Dizi/Film Botu</a>',
  list: '👾 Oyunlar:\n\n<a href="t.me/oyunoynuyoruz">Oyun Grubu</a>\n<a href="t.me/werewolftr">Werewolf Türkiye</a>\n<a href="t.me/secrethitlerTR">Secret Hitler Türkiye</a>\n<a href="t.me/unoTR">UNO TR</a>',
  sticker: '🎴 Sticker:\n\n<a href="https://t.me/addstickers/dizifilm">Dizi/Film</a>\n<a href="https://t.me/addstickers/dizifilm2">Dizi/Film 2</a>\n<a href="https://t.me/addstickers/TurkceBotArsivi">Türkçe Bot Arşivi</a>\n<a href="https://t.me/addstickers/werewolftr">Werewolf Türkiye</a>\n<a href="https://t.me/addstickers/secrethitlertr">Secret Hitler Türkiye</a>\n<a href="https://t.me/addstickers/secrethitlerpack">Secret Hitler TR 2</a>\n<a href="https://t.me/addstickers/sfaysalyildirim">Şerif Faysal Yıldırım</a>\n<a href="https://t.me/addstickers/ozkardesim">Öz Kardeşim</a>\n<a href="https://t.me/addstickers/budamialindi">Ortaya Karışık</a>\n<a href="https://t.me/addstickers/stikermtiker">Ortaya Karışık 2</a>',
  grup: '💢 Rastgele:\n\n<a href="t.me/rastgelefoto">Rastgele Fotoğraflar</a>\n<a href="t.me/rastgelevideo">Rastgele Videolar</a>\n<a href="t.me/rastgelemuzik">Rastgele Müzikler</a>\n<a href="t.me/rastgelegifler">Rastgele Gifler</a>\n<a href="t.me/rastgeleyemek">Rastgele Yemekler</a>\n<a href="t.me/ozkardesim">Öz Kardeşim</a>',
  video: '🏳️‍🌈 Ortaya Karışık:\n\n<a href="t.me/kameraonu">Kadraj</a>\n<a href="t.me/istanbulda">İstanbul Kanalı</a>\n<a href="t.me/podcasttr">Türkçe Podcast</a>\n<a href="t.me/architecturee">Architecture</a>\n<a href="t.me/spamlananlar">Spamlananlar</a>\n<a href="t.me/denemekanali">Deneme Kanalı</a>'
};
